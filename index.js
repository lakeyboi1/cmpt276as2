const express = require('express')
const path = require('path')
const PORT = process.env.PORT || 5000

var app = express()

const { Pool } = require('pg');
var pool = new Pool({
    connectionString: process.env.DATABASE_URL || "postgres://postgres:root@localhost/rectangles",
    ssl: {
        rejectUnauthorized: false
    }
})
  
app.use(express.static(path.join(__dirname, 'public')))
app.use(express.json())
app.use(express.urlencoded({extended:false}))

app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')
app.get('/', (req, res) => res.render('pages/index'))

app.get('/database', async (req,res)=>{
   try {
        const result = await pool.query(`SELECT * FROM rectanglelist`);
        var data = { results : result.rows };
        res.render('pages/db', data);
   }
   catch (error) {
        res.end(error);
   }
})

app.get('/add', async (req,res)=>{
    try {
        const result = await pool.query(`SELECT * FROM rectanglelist`);
        var data = { results : result.rows };
        res.render('pages/add', data);
    }
    catch (error) {
        res.end(error);
    }
})

app.get('/attributes/:name', async (req,res)=>{
    try {
        const result = await pool.query(`SELECT * FROM rectanglelist where name='` + req.params.name + `';`);
        var data = { results : result.rows };
        res.render('pages/attributes', data);
    }
    catch (error) {
        res.end(error);
    }
})

app.get('/delete/:name', async (req,res)=>{
    try {
        const result = await pool.query(`SELECT * FROM rectanglelist where name='` + req.params.name + `';`);
        var data = { results : result.rows };
        res.render('pages/delete', data);
    }
    catch (error) {
        res.end(error);
    }
})

app.post("/add", async (req, res)=>{
    try {
        var properties = req.body;
        if (properties.rectangleName != "" && properties.width != null && properties.height != null && properties.color != "" && properties.favoriteFood != "") {
            await pool.query('INSERT into rectangleList values(\'' + properties.rectangleName + '\',' 
            + properties.width + ',' + properties.height + ',\'' + properties.color + '\',\'' + properties.favoriteFood + '\');');

            res.redirect('/database');
        }
    }
    catch (error) {
        res.end(error);
    }
});

app.post("/attributes/:name", async (req, res)=>{
    try {
        var properties = req.body;
        if (properties.rectangleName != "" && properties.width != null && properties.height != null && properties.color != "" && properties.favoriteFood != "") {
            await pool.query('UPDATE rectangleList SET name=\'' + properties.rectangleName + '\', width=' 
            + properties.width + ', height=' + properties.height + ', color=\'' + properties.color + '\', favorite_food=\'' + properties.favoriteFood 
            + '\' WHERE name=\'' + properties.originalName + '\';');

            res.redirect('/database');
        }
    }
    catch (error) {
        res.end(error);
    }
});

app.post("/delete/:name", async (req, res)=>{
    try {
        var properties = req.body;
        await pool.query('DELETE from rectangleList WHERE name=\'' + properties.rectangleName + '\'');
        res.redirect('/database');
    }
    catch (error) {
        res.end(error);
    }
});

app.listen(PORT, () => console.log(`Listening on ${ PORT }`))